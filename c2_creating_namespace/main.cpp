#include <iostream>
#include "point.h"
#include "number.h"
using namespace std;

namespace Number_Func
{
    int add(int a, int b)
    {
        return a + b;
    }
}

int add(int a, int b)
{
    return a + b + 1;
}

int main(int argc, char *argv[])
{
    int a = 3, b = 4;
    cout << add(a, b) << endl;
    cout << Number_Func::add(a, b) << endl;

    cout << "--- multiply file ---" << endl;
    Math_Func::Number n1;
    Math_Func::Number n2(1.3);
    n1.info_number();
    n2.info_number();
    Math_Func::Point p1;
    Math_Func::Point p2(1.3, 2.3);
    p1.info_point();
    p2.info_point();

    cout << Math_Func::add(a, b) << endl;

    cout << "----- yz ------" << endl;
    return 0;
}