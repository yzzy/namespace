#include <iostream>
#include <string>
#include <memory>
using namespace std;

int main(int argc, char *argv[])
{

    string word{"word"};
    cout << "word : " << word << endl;

    unique_ptr<int> number = make_unique<int>(12);
    cout << number.get() << endl;
    cout << *number << endl;
    cout << "----- yz ------" << endl;
    return 0;
}