#include <iostream>
#include "point.h"
#include "number.h"
using namespace std;
int add(int a, int b);

namespace Number_Func
{
    int add(int a, int b)
    {
        return a + b + 1;
    }
    int add_int(int a, int b)
    {
        return ::add(a, b) + 2;
    }
}

int main(int argc, char *argv[])
{
    int a = 3, b = 4;
    cout << add(a, b) << endl;
    cout << Number_Func::add(a, b) << endl;
    cout << Number_Func::add_int(a, b) << endl;

    cout << "--- multiply file ---" << endl;
    {
        // 只引入函数
        using Number_Func::add_int;
        cout << add_int(a, b) << endl;
    }
    {
        // 只引入类
        using Math_Func::Number;
        Number n1;
        Number n2(1.2);
        n1.info_number();
        n2.info_number();
        Math_Func::Point p1;
    }
    {
        // 引入命名空间并解决冲突
        using namespace Math_Func;
        Number n1;
        Number n2(1.2);
        n1.info_number();
        n2.info_number();
        Point p1;
        Point p2(2.3, 4.5);
        p1.info_point();
        p2.info_point();
        cout << Math_Func::add(a, b) << endl;
    }

    cout << "----- yz ------" << endl;
    return 0;
}

int add(int a, int b)
{
    return a + b;
}