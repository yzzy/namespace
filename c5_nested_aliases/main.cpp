#include <iostream>
#include "point.h"
#include "number.h"
using namespace std;
int add(int a, int b);

namespace Number_Func
{
    int add(int a, int b)
    {
        return a + b + 1;
    }
    int add_int(int a, int b)
    {
        return ::add(a, b) + 2;
    }
    namespace Next_Number
    {
        int add(int a, int b)
        {
            return a + b + 10;
        }
    }
}

int main(int argc, char *argv[])
{
    int a = 3, b = 4;
    cout << Number_Func::Next_Number::add(a, b) << endl;

    {
        namespace Data_Number = Number_Func::Next_Number;
        cout << Data_Number::add(a, b) << endl;
    }

    cout << "----- yz ------" << endl;
    return 0;
}

int add(int a, int b)
{
    return a + b;
}